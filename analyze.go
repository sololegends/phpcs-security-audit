package main

import (
	"io"
	"os"
	"os/exec"
	"strings"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit/plugin"
)

const (
	flagParanoiaMode = "paranoia-mode"

	pathHome        = "/home/php"
	pathCodeSniffer = "./vendor/bin/phpcs"
	pathOutput      = "/tmp/output.json"
	pathRuleset     = "ruleset.xml"
)

var phpExtensions = append(plugin.PhpExtensions, "inc", "lib", "module", "info")

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.BoolFlag{
			Name:   flagParanoiaMode,
			Usage:  "phpcs-security-audit paranoia mode",
			EnvVar: "PHPCS_SECURITY_AUDIT_PARANOIA_MODE",
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {

	// Convert paranoia mode
	paranoiaMode := "0"
	if c.Bool(flagParanoiaMode) {
		paranoiaMode = "1"
	}

	// Run CodeSniffer with phpcs-security-audit rules
	args := []string{
		"--extensions=" + strings.Join(phpExtensions, ","),
		"--standard=" + pathRuleset,
		"--report=json",
		"--report-file=" + pathOutput,
		"--runtime-set", "ParanoiaMode", paranoiaMode,
		path,
	}
	cmd := exec.Command(pathCodeSniffer, args...)
	cmd.Dir = pathHome
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()

	/* Ignore exit code. Here are some possible exit codes:
	- No errors found: 0
	- Errors found, but none of them can be fixed by PHPCBF: 1
	- Errors found, and some can be fixed by PHPCBF: 2
	See https://github.com/squizlabs/PHP_CodeSniffer/blob/master/src/Runner.php */

	return os.Open(pathOutput)
}
