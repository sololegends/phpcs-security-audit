# phpcs-security-audit changelog

## v2.3.0
- Add description to issue (!19)

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!18)

## v2.1.0
- Add support for custom CA certs (!16)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.3.0
- Add `Scanner` property and deprecate `Tool`

## v1.2.0
- Show command error output

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
